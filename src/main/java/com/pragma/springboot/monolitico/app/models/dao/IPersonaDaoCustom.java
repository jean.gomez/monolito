package com.pragma.springboot.monolitico.app.models.dao;

import com.pragma.springboot.monolitico.app.models.entity.Persona;

import java.util.List;

public interface IPersonaDaoCustom {

    public List<Persona> findAllWithFilters(PersonaCriteriaFilter personaCriteriaFilter);

}

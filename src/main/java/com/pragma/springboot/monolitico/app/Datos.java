package com.pragma.springboot.monolitico.app;

import com.pragma.springboot.monolitico.app.models.documents.ImagenMongo;
import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.models.dto.PersonaDto;
import com.pragma.springboot.monolitico.app.models.entity.Imagen;
import com.pragma.springboot.monolitico.app.models.entity.Persona;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Datos {

    public static List<Persona> getListaPersonas() {
        return Arrays.asList(
          new Persona(1L, "Jean", "Gomez", "CC", "109380", "22",
                  "Cucuta", null),
          new Persona(2L, "Jose", "Perez", "CC", "108931", "22",
                  "Medellin", null),
          new Persona(3L, "Luis", "Zapata", "CC", "109512", "22",
                  "Bogota", null),
          new Persona(4L, "Miguel", "Rogelio", "CC", "12145", "22",
                  "Cucuta", null),
          new Persona(5L, "Andres", "Marcos", "CC", "102381", "22",
                  "Bucaramanga", null)
        );
    }

    public static List<Persona> getListaPersonasCriteria() {
        return List.of(
                new Persona(1L, "Jean", "Gomez", "CC", "109380", "22",
                        "Cucuta", null)
        );
    }

    public static PersonaDto getPersonaDto() {
        return new PersonaDto(null, "Jean", "Gomez", "CC", "109380",
                "22", "Cucuta", null);
    }

    public static PersonaDto getPersonaDtoConImagen() {
        ImagenDto imagenDto = new ImagenDto(null, "42342352342342", 1L);
        return new PersonaDto(null, "Jean", "Gomez", "CC", "109380",
                "22", "Cucuta", List.of(imagenDto));
    }

    public static Optional<Persona> getPersona() {
        return Optional.of(new Persona(1L, "Jean", "Gomez", "CC", "109380",
                "22", "Cucuta", null));
    }

    public static Persona crearPersona() {
        return new Persona(1L, "Jean", "Gomez", "CC", "109380",
                "22", "Cucuta", null);
    }


    public static List<ImagenDto> getListaImagenesDto() {
        return Arrays.asList(
                new ImagenDto("1", "gsf234vsr24121", 1L),
                new ImagenDto("2", "gw4234gsdr2342f", 1L),
                new ImagenDto("3", "gdsf234sadf2421", 1L)
        );
    }

    public static List<Imagen> getListaImagenes() {
        return Arrays.asList(
                new Imagen(1L, new Persona(1L), "gsf234vsr24121"),
                new Imagen(2L, new Persona(1L), "gw4234gsdr2342f"),
                new Imagen(3L, new Persona(2L), "gdsf234sadf2421"),
                new Imagen(4L, new Persona(3L), "gdsf234sadf2421"),
                new Imagen(5L, new Persona(3L), "gdsf234sadf2421"),
                new Imagen(6L, new Persona(3L), "gdsf234sadf2421")
        );
    }

    public static List<ImagenMongo> getListaImagenesMongo() {
        return Arrays.asList(
                new ImagenMongo("1234", "1", "gsf234vsr24121"),
                new ImagenMongo("1235", "1", "gw4234gsdr2342f"),
                new ImagenMongo("1236", "2", "gdsf234sadf2421"),
                new ImagenMongo("1237", "3", "gdsf234sadf2421"),
                new ImagenMongo("1238", "3", "gdsf234sadf2421"),
                new ImagenMongo("1239", "3", "gdsf234sadf2421")
        );
    }

    public static List<ImagenMongo> getListaImagenesMongoPersona() {
        return Arrays.asList(
                new ImagenMongo("1236", "3", "gdsf234sadf2421"),
                new ImagenMongo("1237", "3", "gdsf234sadf2421"),
                new ImagenMongo("1238", "3", "gdsf234sadf2421")
        );
    }

    public static Optional<Imagen> getImagen() {
        return Optional.of(new Imagen(1L, new Persona(1L), "gsf234vsr24121"));
    }

    public static Optional<ImagenMongo> getImagenMongo() {
        return Optional.of(new ImagenMongo("1234", "1", "gsf234vsr24121"));
    }

    public static ImagenDto getImagenDto() {
        return new ImagenDto(null, "42342352342342", 1L);
    }

    public static ImagenDto crearImagenDto() {
        return new ImagenDto(null, "42342352342342", 1L);
    }

    public static ImagenMongo crearImagenMongo() {
        return new ImagenMongo("123456", "1", "42342352342342");
    }

    public static Imagen crearImagen() {
        return new Imagen(1L, new Persona(1L), "gsf234vsr24121");
    }

}

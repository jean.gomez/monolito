package com.pragma.springboot.monolitico.app.services.impl;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.pragma.springboot.monolitico.app.exceptions.ImageNotFoundException;
import com.pragma.springboot.monolitico.app.models.entity.Persona;
import com.pragma.springboot.monolitico.app.services.IImagenService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pragma.springboot.monolitico.app.models.dao.IImagenDao;
import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.models.entity.Imagen;

@Service
@Qualifier("mysql")
public class ImagenServiceImpl implements IImagenService {

    @Autowired
    IImagenDao imagenDao;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<ImagenDto> findAll() {
        List<Imagen> listaImagenes = (List<Imagen>) imagenDao.findAll();
        return listImagenToListImagenDto(listaImagenes);
    }

    @Override
    public ImagenDto findById(String id) {
        Imagen imagen = imagenDao.findById(Long.parseLong(id)).orElse(null);
        if (imagen != null) {
            return imagenToImagenDto(imagen);
        }
        return null;
    }

    @Override
    public List<ImagenDto> findByIdPersona(Long idPersona) {
        Persona persona = new Persona();
        persona.setId(idPersona);
        List<Imagen> listaImagenes = imagenDao.findByPersona(persona);
        return listImagenToListImagenDto(listaImagenes);
    }

    @Override
    public ImagenDto save(ImagenDto imagenDto) {
        Persona persona = new Persona(imagenDto.getIdPersona());
        Imagen imagen = new Imagen(persona, imagenDto.getBase64());
        Imagen imagenGuardada = imagenDao.save(imagen);
        return imagenToImagenDto(imagenGuardada);
    }

    @Override
    public ImagenDto update(ImagenDto imagenDto, String id) {
        Imagen imagen = imagenDtoToImagen(imagenDto);
        imagen.setId(Long.parseLong(id));
        Imagen imagenBd = imagenDao.findById(Long.parseLong(id)).orElseThrow(() ->
                new ImageNotFoundException("La imagen que esta inteando actualizar no existe"));
        imagenBd.setBase64(imagenDto.getBase64());
        return imagenToImagenDto(imagenDao.save(imagenBd));
    }

    @Override
    public List<ImagenDto> saveAll(List<ImagenDto> imagenesDto) {
        if (!imagenesDto.isEmpty()) {
            List<Imagen> imagenesGuardadas = (List<Imagen>) imagenDao.saveAll(listImagenDtoToListImagen(imagenesDto));
            return listImagenToListImagenDto(imagenesGuardadas);
        }
        return Collections.emptyList();
    }

    @Override
    public void delete(String id) {
        try {
            imagenDao.deleteById(Long.parseLong(id));
        } catch (Exception e) {
            throw new RuntimeException("Ha ocurrido un error al eliminar esta imagen");
        }
    }

    @Override
    public void deleteByIdPersona(Long idPersona) {
        if (idPersona == null) {
            throw new IllegalArgumentException("El id de la persona no puede ser null");
        }
        try {
            Persona persona = new Persona(idPersona);
            imagenDao.deleteByPersona(persona);
        } catch (Exception e) {
            throw new RuntimeException("Ha ocurrido un error al eliminar las imagenes de esta persona");
        }
    }

    public ImagenDto imagenToImagenDto(Imagen imagen) {
        ImagenDto imagenDto = new ImagenDto(imagen.getId().toString(), imagen.getBase64(), imagen.getPersona().getId());
        return imagenDto;
    }

    public Imagen imagenDtoToImagen(ImagenDto imagenDto) {
        Persona persona = new Persona();
        persona.setId(imagenDto.getIdPersona());
        Imagen imagen = new Imagen(persona, imagenDto.getBase64());
        return imagen;
    }

    public List<ImagenDto> listImagenToListImagenDto(List<Imagen> imagenes) {
        return imagenes.stream().map(this::imagenToImagenDto).collect(Collectors.toList());
    }

    public List<Imagen> listImagenDtoToListImagen(List<ImagenDto> imagenes) {
        return imagenes.stream().map(this::imagenDtoToImagen).collect(Collectors.toList());
    }

}

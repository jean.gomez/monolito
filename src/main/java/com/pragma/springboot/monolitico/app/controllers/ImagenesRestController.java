package com.pragma.springboot.monolitico.app.controllers;

import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.services.IImagenService;
import com.pragma.springboot.monolitico.app.utils.Estados;
import com.pragma.springboot.monolitico.app.utils.Mensajes;
import com.pragma.springboot.monolitico.app.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/api/imagenes")
public class ImagenesRestController {

    @Autowired
    @Qualifier("mongo")
    IImagenService imagenService;

    @GetMapping("/")
    public ResponseEntity<Response> list() {
        return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, imagenService.findAll());
    }

    @GetMapping("/by-user-id/{id-persona}")
    public ResponseEntity<Response> listByPersonaId(
            @PathVariable(name = "id-persona") Long idPersona) {
        return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, imagenService.findByIdPersona(idPersona));
    }

    @PostMapping("/")
    public ResponseEntity<Response> create(@Valid @RequestBody ImagenDto imagenDto, BindingResult result) {
        if (result.hasErrors()) {
            return validateBinding(result);
        }
        return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, imagenService.save(imagenDto));
    }

    @PostMapping("/bulk")
    public ResponseEntity<Response> createBulk(@Valid @RequestBody List<ImagenDto> imagenesDto, BindingResult result) {
        if (result.hasErrors()) {
            return validateBinding(result);
        }
        return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, imagenService.saveAll(imagenesDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Response> update(
            @Valid @RequestBody ImagenDto imagenDto, BindingResult result, @PathVariable String id) {
        if (result.hasErrors()) {
            return validateBinding(result);
        }
        return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, imagenService.update(imagenDto, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> delete(@PathVariable String id) {
        imagenService.delete(id);
        return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, Mensajes.SUCCESS);
    }

    private ResponseEntity<Response> validateBinding(BindingResult result) {
        List<String> mensajes = result.getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage).toList();
        return badRequest(Mensajes.BAD_REQUEST, Estados.BAD_REQUEST, mensajes);
    }

    private ResponseEntity<Response> okRequest(String mensaje, String status, Object data) {
        return ResponseEntity.ok(
                Response.builder()
                        .mensaje(mensaje)
                        .status(status)
                        .data(data)
                        .build()
        );
    }

    private ResponseEntity<Response> badRequest(String mensaje, String status, Object data) {
        return ResponseEntity.badRequest().body(
                Response.builder()
                        .mensaje(mensaje)
                        .status(status)
                        .data(data)
                        .build()
        );
    }

}

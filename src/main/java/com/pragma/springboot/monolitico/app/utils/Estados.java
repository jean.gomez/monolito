package com.pragma.springboot.monolitico.app.utils;

public class Estados {

    public static final String SUCCESS = "200";
    public static final String BAD_REQUEST = "400";
    public static final String NOT_FOUND = "404";
    public static final String INTERNAL_SERVER_ERROR = "500";

}

package com.pragma.springboot.monolitico.app.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersonaCriteriaFilter {
    private String tipoDocumento;
    private String documento;
    private Integer mayorIgualA;
}

package com.pragma.springboot.monolitico.app.models.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.Type;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Imagen implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "id_persona", referencedColumnName = "id")
	private Persona persona;

	@Type(type = "text")
	private String base64;

	public Imagen(Persona persona, String base64) {
		this.persona = persona;
		this.base64 = base64;
	}
}

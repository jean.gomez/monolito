package com.pragma.springboot.monolitico.app.utils;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class Response {

    private Object data;
    private String mensaje;
    private String status;

}

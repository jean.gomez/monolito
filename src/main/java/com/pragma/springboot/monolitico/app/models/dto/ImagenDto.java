package com.pragma.springboot.monolitico.app.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class ImagenDto {

	private String id;
	@NotBlank(message = "El base64 no puede estar vacio")
	private String base64;
	@NotNull(message = "El id de la persona, no puede ser vacia")
	private Long idPersona;
}

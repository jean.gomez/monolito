package com.pragma.springboot.monolitico.app.utils;

public class Mensajes {

    public static final String SUCCESS = "El proceso ha sido exitoso";
    public static final String BAD_REQUEST = "Solicitud incorrecta";
    public static final String INTERNAL_SERVER_ERROR = "Ha ocurrido un error interno";

}

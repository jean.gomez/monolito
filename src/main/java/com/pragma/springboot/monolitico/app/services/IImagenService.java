package com.pragma.springboot.monolitico.app.services;

import java.awt.*;
import java.util.List;

import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;

public interface IImagenService {

	public List<ImagenDto> findAll();
	
	public ImagenDto findById(String id);

	public List<ImagenDto> findByIdPersona(Long idPersona);
	
	public ImagenDto save(ImagenDto imagenDto);

	public ImagenDto update(ImagenDto imagenDto, String id);
	
	public List<ImagenDto> saveAll(List<ImagenDto> imagenes);
	
	public void delete(String id);

	public void deleteByIdPersona(Long idPersona);

}

package com.pragma.springboot.monolitico.app.services;

import java.util.List;

import com.pragma.springboot.monolitico.app.models.dao.PersonaCriteriaFilter;
import com.pragma.springboot.monolitico.app.models.dto.PersonaDto;

public interface IPersonaService {

	public List<PersonaDto> findAll();

	public List<PersonaDto> findByFilter(PersonaCriteriaFilter personaFiltro);
	
	public PersonaDto findById(Long id);
	
	public PersonaDto save(PersonaDto personaDto);

	public PersonaDto update(PersonaDto personaDto, Long id);
	
	public void delete(Long id);
	
}

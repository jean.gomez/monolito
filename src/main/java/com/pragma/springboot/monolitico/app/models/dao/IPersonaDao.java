package com.pragma.springboot.monolitico.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.pragma.springboot.monolitico.app.models.entity.Persona;

import java.util.List;

public interface IPersonaDao extends CrudRepository<Persona, Long> {

}

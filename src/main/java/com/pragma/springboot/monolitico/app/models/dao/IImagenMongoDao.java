package com.pragma.springboot.monolitico.app.models.dao;

import com.pragma.springboot.monolitico.app.models.documents.ImagenMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IImagenMongoDao extends MongoRepository<ImagenMongo, String> {

    public List<ImagenMongo> findByIdPersona(String id);

    public void deleteByIdPersona(String id);

}

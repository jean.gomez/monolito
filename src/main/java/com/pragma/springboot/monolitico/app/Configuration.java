package com.pragma.springboot.monolitico.app;

import com.pragma.springboot.monolitico.app.models.dto.PersonaMapper;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	}

	@Bean
	public PersonaMapper getPersonaMapper() {
		return new PersonaMapper();
	}

}

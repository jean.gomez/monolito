package com.pragma.springboot.monolitico.app.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDto {
	
	private Long id;
	@NotBlank(message = "El campo nombres no puede ser vacio")
	private String nombres;
	@NotBlank(message = "El campo apellidos no puede ser vacio")
	private String apellidos;
	@NotBlank(message = "El campo tipo documento no puede ser vacio")
	private String tipoDocumento;
	@NotBlank(message = "El campo documento no puede ser vacio")
	private String documento;
	@Min(value = 0, message = "El campo edad no puede ser un valor negativo")
	@NotBlank(message = "El campo edad no puede ser vacio")
	private String edad;
	private String ciudadNacimiento;
	private List<ImagenDto> imagenes;

}

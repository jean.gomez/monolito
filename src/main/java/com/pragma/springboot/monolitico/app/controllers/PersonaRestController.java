package com.pragma.springboot.monolitico.app.controllers;

import com.pragma.springboot.monolitico.app.models.dao.PersonaCriteriaFilter;
import com.pragma.springboot.monolitico.app.utils.Estados;
import com.pragma.springboot.monolitico.app.utils.Mensajes;
import com.pragma.springboot.monolitico.app.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.pragma.springboot.monolitico.app.models.dto.PersonaDto;
import com.pragma.springboot.monolitico.app.services.IPersonaService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/api/personas")
public class PersonaRestController {

	@Autowired
	private IPersonaService personaService;

	@GetMapping({"/", ""})
	public ResponseEntity<Response> filter(
			@RequestParam(name = "tipo-documento", required = false) String tipoDocumento,
		    @RequestParam(name = "documento", required = false) String documento,
		    @RequestParam(name = "edad", required = false) Integer edad) {
		return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, personaService.findByFilter(new PersonaCriteriaFilter(tipoDocumento, documento, edad)));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Response> show(@PathVariable Long id) {
		return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, personaService.findById(id));
	}

	@PostMapping("/")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Response> create(@Valid @RequestBody PersonaDto personaDto, BindingResult result) {
		if (result.hasErrors()) {
			return  validateBinding(result);
		}
 		return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, personaService.save(personaDto));
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Response> update(@Valid @RequestBody PersonaDto personaDto, BindingResult result, @PathVariable Long id) {
		if (result.hasErrors()) {
			validateBinding(result);
		}
		return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, personaService.update(personaDto, id));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Response> delete(@PathVariable Long id) {
		personaService.delete(id);
		return okRequest(Mensajes.SUCCESS, Estados.SUCCESS, Mensajes.SUCCESS);
	}

	private ResponseEntity<Response> validateBinding(BindingResult result) {
		List<String> mensajes = result.getAllErrors()
				.stream()
				.map(DefaultMessageSourceResolvable::getDefaultMessage).toList();
		return badRequest(Mensajes.BAD_REQUEST, Estados.BAD_REQUEST, mensajes);
	}

	private ResponseEntity<Response> okRequest(String mensaje, String status, Object data) {
		return ResponseEntity.ok(
				Response.builder()
						.mensaje(mensaje)
						.status(status)
						.data(data)
						.build()
		);
	}

	private ResponseEntity<Response> badRequest(String mensaje, String status, Object data) {
		return ResponseEntity.badRequest().body(
				Response.builder()
						.mensaje(mensaje)
						.status(status)
						.data(data)
						.build()
		);
	}

}

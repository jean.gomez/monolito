package com.pragma.springboot.monolitico.app.models.dto;

import com.pragma.springboot.monolitico.app.models.entity.Persona;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class PersonaMapper {

    @Autowired
    ModelMapper modelMapper;

    public PersonaDto mapPersonaToPersonaDto(Persona persona) {
        return modelMapper.map(persona, PersonaDto.class);
    }

    public List<PersonaDto> mapListPersonaToListPersonaDto(List<Persona> personas) {
        return personas.stream().map(this::mapPersonaToPersonaDto).collect(Collectors.toList());
    }

    public Persona mapPersonaDtoToPersona(PersonaDto personaDto) {
        return modelMapper.map(personaDto, Persona.class);
    }

}

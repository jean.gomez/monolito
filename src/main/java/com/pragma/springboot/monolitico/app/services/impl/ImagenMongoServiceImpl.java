package com.pragma.springboot.monolitico.app.services.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.pragma.springboot.monolitico.app.exceptions.ImageNotFoundException;
import com.pragma.springboot.monolitico.app.models.dao.IImagenMongoDao;
import com.pragma.springboot.monolitico.app.models.documents.ImagenMongo;
import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.services.IImagenService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("mongo")
public class ImagenMongoServiceImpl implements IImagenService {

	@Autowired
	IImagenMongoDao imagenMongoDao;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public List<ImagenDto> findAll() {
		List<ImagenMongo> listaImagenes = imagenMongoDao.findAll();
		return listImagenToListImagenDto(listaImagenes);
	}

	@Override
	public ImagenDto findById(String id) {
		ImagenDto imagenDto = null;
		Optional<ImagenMongo> imagenMongoOptional = imagenMongoDao.findById(id);
		if (imagenMongoOptional.isPresent()) {
			ImagenMongo imagenMongo = imagenMongoOptional.get();
			imagenDto = imagenToImagenDto(imagenMongo);
		}
		return imagenDto;
	}

	@Override
	public List<ImagenDto> findByIdPersona(Long idPersona) {
		List<ImagenMongo> listaImagenes =  imagenMongoDao.findByIdPersona(idPersona.toString());
		return listImagenToListImagenDto(listaImagenes);
	}

	@Override
	public ImagenDto save(ImagenDto imagenDto) {
		ImagenMongo imagen = imagenDtoToImagen(imagenDto);
		return imagenToImagenDto(imagenMongoDao.save(imagen));
	}

	@Override
	public ImagenDto update(ImagenDto imagenDto, String id) {
		ImagenMongo imagen = imagenDtoToImagen(imagenDto);
		imagen.set_id(id);
		ImagenMongo imagenBd = imagenMongoDao.findById(id).orElseThrow(() ->
				new ImageNotFoundException("La imagen que esta inteando actualizar no existe"));
		imagenBd.setBase64(imagenDto.getBase64());
		return imagenToImagenDto(imagenMongoDao.save(imagenBd));
	}

	@Override
	public List<ImagenDto> saveAll(List<ImagenDto> imagenesDto) {
		if (!imagenesDto.isEmpty()) {
			List<ImagenMongo> imagenesGuardadas = imagenMongoDao.saveAll(listImagenDtoToListImagen(imagenesDto));
			return listImagenToListImagenDto(imagenesGuardadas);
		}
		return Collections.emptyList();
	}

	@Override
	public void delete(String id) {
		try {
			imagenMongoDao.deleteById(id);
		} catch (Exception e) {
			throw new RuntimeException("Ha ocurrido un error al eliminar esta imagen");
		}
	}

	@Override
	public void deleteByIdPersona(Long idPersona) {
		try {
			imagenMongoDao.deleteByIdPersona(idPersona.toString());
		} catch (Exception e) {
			throw new RuntimeException("Ha ocurrido un error al eliminar las imagenes de esta persona");
		}
	}

	public ImagenDto imagenToImagenDto(ImagenMongo imagen) {
		ImagenDto imagenDto = new ImagenDto(imagen.get_id(), imagen.getBase64(),
				Long.parseLong(imagen.getIdPersona()));
		return imagenDto;
	}

	public ImagenMongo imagenDtoToImagen(ImagenDto imagenDto) {
		ImagenMongo imagen = new ImagenMongo(imagenDto.getIdPersona().toString(), imagenDto.getBase64());
		return imagen;
	}

	public List<ImagenDto> listImagenToListImagenDto(List<ImagenMongo> imagenes) {
		return imagenes.stream().map(this::imagenToImagenDto).collect(Collectors.toList());
	}

	public List<ImagenMongo> listImagenDtoToListImagen(List<ImagenDto> imagenes) {
		return imagenes.stream().map(this::imagenDtoToImagen).collect(Collectors.toList());
	}

}

package com.pragma.springboot.monolitico.app.models.dao;

import com.pragma.springboot.monolitico.app.models.entity.Persona;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class PersonaCriteriaImpl implements IPersonaDaoCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Persona> findAllWithFilters(PersonaCriteriaFilter personaCriteriaFilter) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Persona> criteriaQuery = criteriaBuilder.createQuery(Persona.class);
        Root<Persona> personaRoot = criteriaQuery.from(Persona.class);
        Predicate predicate = getPredicate(personaCriteriaFilter, personaRoot);
        criteriaQuery.where(predicate);
        TypedQuery<Persona> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    private Predicate getPredicate(PersonaCriteriaFilter personaCriteriaFilter, Root<Persona> personaRoot) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        List<Predicate> predicates = new ArrayList<>();
        if (Objects.nonNull(personaCriteriaFilter.getTipoDocumento())) {
            predicates.add(criteriaBuilder.equal(personaRoot.get("tipoDocumento"),
                    personaCriteriaFilter.getTipoDocumento()));
        }

        if (Objects.nonNull(personaCriteriaFilter.getDocumento())) {
            predicates.add(criteriaBuilder.equal(personaRoot.get("documento"), personaCriteriaFilter.getDocumento()));
        }

        if (Objects.nonNull(personaCriteriaFilter.getMayorIgualA())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(personaRoot.get("edad"),
                    personaCriteriaFilter.getMayorIgualA()));
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}

package com.pragma.springboot.monolitico.app.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "personas")
public class Persona implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nombres;
	private String apellidos;
	private String tipoDocumento;
	private String documento;
	private String edad;
	private String ciudadNacimiento;

	@JsonManagedReference
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_persona", referencedColumnName = "id")
	private List<Imagen> foto;

	public Persona(Long id) {
		this.id = id;
	}
}

package com.pragma.springboot.monolitico.app.services.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.pragma.springboot.monolitico.app.exceptions.UserNotFoundException;
import com.pragma.springboot.monolitico.app.models.dao.IPersonaDaoCustom;
import com.pragma.springboot.monolitico.app.models.dao.PersonaCriteriaFilter;
import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.models.dto.PersonaMapper;
import com.pragma.springboot.monolitico.app.services.IImagenService;
import com.pragma.springboot.monolitico.app.services.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.pragma.springboot.monolitico.app.models.dao.IPersonaDao;
import com.pragma.springboot.monolitico.app.models.dto.PersonaDto;
import com.pragma.springboot.monolitico.app.models.entity.Persona;

@Service
public class PersonaServiceImpl implements IPersonaService {

    @Autowired
    private IPersonaDao personaDao;

    @Autowired
    private IPersonaDaoCustom personaDaoCustom;

    @Autowired
    @Qualifier("mongo")
    private IImagenService imagenService;

    @Autowired
    private PersonaMapper personaMapper;

    @Override
    public List<PersonaDto> findAll() {
        List<Persona> listaPersonas = (List<Persona>) personaDao.findAll();
        List<PersonaDto> listaPersonaDtos = personaMapper.mapListPersonaToListPersonaDto(listaPersonas);
        cargarImagenesListaPersonas(listaPersonaDtos);
        return listaPersonaDtos;
    }

    @Override
    public List<PersonaDto> findByFilter(PersonaCriteriaFilter personaFiltro) {
        List<Persona> listaPersonas = personaDaoCustom.findAllWithFilters(personaFiltro);
        List<PersonaDto> listaPersonaDtos = personaMapper.mapListPersonaToListPersonaDto(listaPersonas);
        cargarImagenesListaPersonas(listaPersonaDtos);
        return listaPersonaDtos;
    }

    @Override
    public PersonaDto findById(Long id) {
        PersonaDto personaDto = null;
        Optional<Persona> personaOptional = personaDao.findById(id);
        if (personaOptional.isPresent()) {
            Persona persona = personaOptional.get();
            List<ImagenDto> imagenes = imagenService.findByIdPersona(persona.getId());
            personaDto = personaMapper.mapPersonaToPersonaDto(persona);
            personaDto.setImagenes(imagenes);
        }
        return personaDto;
    }

    @Override
    public PersonaDto save(PersonaDto personaDto) {
        Persona persona = personaMapper.mapPersonaDtoToPersona(personaDto);
        PersonaCriteriaFilter filter =
                new PersonaCriteriaFilter(persona.getTipoDocumento(), persona.getDocumento(), null);
        List<Persona> personasDb = personaDaoCustom.findAllWithFilters(filter);
        if (personasDb != null && personasDb.size() > 0) {
            throw new DuplicateKeyException("Ya existe una persona registrada con este documento");
        }
        PersonaDto personaCreada = personaMapper.mapPersonaToPersonaDto(personaDao.save(persona));
        List<ImagenDto> imagenes = personaDto.getImagenes();
        if (imagenes != null && !imagenes.isEmpty()) {
            imagenes.get(0).setIdPersona(personaCreada.getId());
            personaCreada.setImagenes(List.of(imagenService.save(imagenes.get(0))));
        } else {
            personaCreada.setImagenes(Collections.emptyList());
        }
        return personaCreada;
    }

    @Override
    public PersonaDto update(PersonaDto personaDto, Long id) {
        Persona persona = personaMapper.mapPersonaDtoToPersona(personaDto);
        persona.setId(id);
        Persona personaBd = personaDao.findById(id).orElseThrow(() ->
                new UserNotFoundException("La persona que esta intentando editar no existe"));
        personaBd.setApellidos(personaDto.getApellidos());
        personaBd.setNombres(personaDto.getNombres());
        personaBd.setCiudadNacimiento(personaDto.getCiudadNacimiento());
        personaBd.setTipoDocumento(personaBd.getTipoDocumento());
        return personaMapper.mapPersonaToPersonaDto(personaDao.save(personaBd));
    }

    @Override
    public void delete(Long id) {
        try {
            personaDao.deleteById(id);
            imagenService.deleteByIdPersona(id);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void cargarImagenesListaPersonas(List<PersonaDto> listaPersonas) {
        for (PersonaDto personaDto : listaPersonas) {
            List<ImagenDto> imagenes = imagenService.findByIdPersona(personaDto.getId());
            personaDto.setImagenes(imagenes);
        }
    }

}

package com.pragma.springboot.monolitico.app.exceptions;

import java.time.LocalDate;

public class Error {

    private final String nombreExcepcion;
    private final String mensaje;
    private final LocalDate timeStamp;

    public Error(String nombreExcepcion, String mensaje, LocalDate timeStamp) {
        this.nombreExcepcion = nombreExcepcion;
        this.mensaje = mensaje;
        this.timeStamp = timeStamp;
    }

    public String getNombreExcepcion() {
        return nombreExcepcion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public LocalDate getTimeStamp() {
        return timeStamp;
    }
}

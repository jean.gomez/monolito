package com.pragma.springboot.monolitico.app.models.dao;

import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.models.dto.PersonaDto;
import com.pragma.springboot.monolitico.app.models.entity.Persona;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pragma.springboot.monolitico.app.models.entity.Imagen;

import java.util.List;

@Repository
public interface IImagenDao extends CrudRepository<Imagen, Long> {

    public List<Imagen> findByPersona(Persona persona);

    public void deleteByPersona(Persona persona);

}

package com.pragma.springboot.monolitico.app.models.documents;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "imagenes")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImagenMongo {

	@Id
	private String _id;
	private String idPersona;
	private String base64;

	public ImagenMongo(String idPersona, String base64) {
		this.idPersona = idPersona;
		this.base64 = base64;
	}
}

package com.pragma.springboot.monolitico.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDate;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    private static final String ERROR = "Ha ocurrido un error interno, por favor contacte al administrador.";
    private static final ConcurrentHashMap<String, Integer> CODIGOS_ESTADO = new ConcurrentHashMap<>();

    public ControllerAdvisor() {
        CODIGOS_ESTADO.put("UserNotFoundException", HttpStatus.INTERNAL_SERVER_ERROR.value());
        CODIGOS_ESTADO.put("ImageNotFoundException", HttpStatus.INTERNAL_SERVER_ERROR.value());
        CODIGOS_ESTADO.put("DuplicateKeyException", HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Error> handleAllExceptions(Exception e) {
        ResponseEntity<Error> resultado;

        String excepcionNombre = e.getClass().getSimpleName();
        String mensaje = e.getMessage();
        Integer codigo = CODIGOS_ESTADO.get(excepcionNombre);

        if (codigo != null) {
            Error error = new Error(excepcionNombre, mensaje, LocalDate.now());
            resultado = new ResponseEntity<>(error, HttpStatus.valueOf(codigo));
        }
        else {
            Error error = new Error(excepcionNombre, ERROR, LocalDate.now());
            resultado = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resultado;
    }

}

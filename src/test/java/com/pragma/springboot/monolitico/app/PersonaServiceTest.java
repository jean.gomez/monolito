package com.pragma.springboot.monolitico.app;

import com.pragma.springboot.monolitico.app.exceptions.UserNotFoundException;
import com.pragma.springboot.monolitico.app.models.dao.IPersonaDao;
import com.pragma.springboot.monolitico.app.models.dao.IPersonaDaoCustom;
import com.pragma.springboot.monolitico.app.models.dao.PersonaCriteriaFilter;
import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.models.dto.PersonaDto;
import com.pragma.springboot.monolitico.app.models.entity.Persona;
import com.pragma.springboot.monolitico.app.services.IImagenService;
import com.pragma.springboot.monolitico.app.services.IPersonaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DuplicateKeyException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.pragma.springboot.monolitico.app.Datos.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class PersonaServiceTest {

    @MockBean
    IPersonaDaoCustom personaDaoCustom;
    @MockBean
    IPersonaDao personaDao;

    @Autowired
    IPersonaService service;

    @MockBean
    @Qualifier("mongo")
    IImagenService imagenService;

    PersonaCriteriaFilter criteria;

    @Test
    void testFindAllSinFiltro() {
        criteria = new PersonaCriteriaFilter(null, null, null);
        when(personaDaoCustom.findAllWithFilters(criteria)).thenReturn(getListaPersonas());
        when(imagenService.findByIdPersona(anyLong())).thenReturn(new ArrayList<>());
        when(imagenService.findByIdPersona(1L)).thenReturn(getListaImagenesDto());

        List<PersonaDto> personasPorFiltro = service.findByFilter(criteria);
        assertFalse(personasPorFiltro.isEmpty());
        assertFalse(personasPorFiltro.get(0).getImagenes().isEmpty());
        assertTrue(personasPorFiltro.get(1).getImagenes().isEmpty());
    }

    @Test
    void testFindById() {
        when(personaDao.findById(1L)).thenReturn(getPersona());
        when(imagenService.findByIdPersona(1L)).thenReturn(getListaImagenesDto());
        when(personaDao.findById(10L)).thenReturn(Optional.empty());

        PersonaDto personaDto = service.findById(1L);
        assertEquals(1L, personaDto.getId());
        assertFalse(personaDto.getImagenes().isEmpty());

        personaDto = service.findById(10L);
        assertNull(personaDto);
    }

    @Test
    void testSaveDuplicate() {
        when(personaDaoCustom.findAllWithFilters(any(PersonaCriteriaFilter.class))).thenReturn(getListaPersonasCriteria());
        assertThrows(DuplicateKeyException.class, () -> {
            service.save(getPersonaDto());
        });
    }

    @Test
    void testSave() {
        when(personaDaoCustom.findAllWithFilters(any(PersonaCriteriaFilter.class))).thenReturn(new ArrayList<>());
        when(personaDao.save(any(Persona.class))).thenReturn(crearPersona());
        when(imagenService.save(any(ImagenDto.class))).thenReturn(crearImagenDto());

        PersonaDto personaDto = service.save(getPersonaDtoConImagen());
        assertEquals("Jean", personaDto.getNombres());
        assertFalse(personaDto.getImagenes().isEmpty());

        verify(imagenService).save(any(ImagenDto.class));
    }

    @Test
    void testSaveSinImagen() {
        when(personaDaoCustom.findAllWithFilters(any(PersonaCriteriaFilter.class))).thenReturn(new ArrayList<>());
        when(personaDao.save(any(Persona.class))).thenReturn(crearPersona());

        PersonaDto personaDto = service.save(getPersonaDto());
        assertEquals("Jean", personaDto.getNombres());
        assertTrue(personaDto.getImagenes().isEmpty());

        verify(imagenService, never()).save(any(ImagenDto.class));
    }

    @Test
    void testUpdate() {
        when(personaDao.findById(1L)).thenReturn(getPersona());
        when(personaDao.findById(10L)).thenReturn(Optional.empty());
        when(personaDao.save(any(Persona.class))).thenReturn(crearPersona());

        PersonaDto personaDto = service.update(getPersonaDto(), 1L);
        assertEquals("Jean", personaDto.getNombres());

        verify(personaDao).findById(1L);
        verify(personaDao).save(any(Persona.class));

        assertThrows(UserNotFoundException.class, () -> {
            service.update(getPersonaDto(), 10L);
        });
    }

    @Test
    void testDelete() {
        doNothing().when(personaDao).deleteById(anyLong());
        doThrow(RuntimeException.class).when(personaDao).deleteById(isNull());

        assertThrows(RuntimeException.class, () -> {
            service.delete(null);
        });

        service.delete(1L);

        verify(imagenService).deleteByIdPersona(anyLong());
    }
}

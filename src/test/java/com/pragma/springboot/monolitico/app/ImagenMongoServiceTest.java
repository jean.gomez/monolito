package com.pragma.springboot.monolitico.app;

import com.pragma.springboot.monolitico.app.exceptions.ImageNotFoundException;
import com.pragma.springboot.monolitico.app.models.dao.IImagenMongoDao;
import com.pragma.springboot.monolitico.app.models.documents.ImagenMongo;
import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.services.IImagenService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static com.pragma.springboot.monolitico.app.Datos.*;

@SpringBootTest
public class ImagenMongoServiceTest {

    @MockBean
    IImagenMongoDao imagenDao;

    @Autowired
    @Qualifier("mongo")
    IImagenService service;

    @Test
    void testFind() {
        when(imagenDao.findAll()).thenReturn(getListaImagenesMongo());
        when(imagenDao.findById(anyString())).thenReturn(Optional.empty());
        when(imagenDao.findById("1")).thenReturn(getImagenMongo());
        when(imagenDao.findByIdPersona(any())).thenReturn(Collections.emptyList());
        when(imagenDao.findByIdPersona("3")).thenReturn(getListaImagenesMongoPersona());

        List<ImagenDto> imagenes = service.findAll();
        assertNotNull(imagenes);
        assertFalse(imagenes.isEmpty());

        ImagenDto imagen = service.findById("1");
        assertNotNull(imagen);

        ImagenDto imagenNull = service.findById("15");
        assertNull(imagenNull);

        List<ImagenDto> imagenesPersona = service.findByIdPersona(3L);
        assertNotNull(imagenesPersona);
        assertFalse(imagenesPersona.isEmpty());

        List<ImagenDto> listaSinImagenes = service.findByIdPersona(35L);
        assertTrue(listaSinImagenes.isEmpty());
    }

    @Test
    void testSave() {
        when(imagenDao.save(any(ImagenMongo.class))).thenReturn(crearImagenMongo());
        when(imagenDao.saveAll(anyList())).thenReturn(getListaImagenesMongo());

        ImagenDto imagenDto = service.save(getImagenDto());
        assertNotNull(imagenDto);
        verify(imagenDao).save(any(ImagenMongo.class));

        List<ImagenDto> imagenDtos = service.saveAll(getListaImagenesDto());
        assertFalse(imagenDtos.isEmpty());

        List<ImagenDto> listaVacia = service.saveAll(new ArrayList<>());
        assertTrue(listaVacia.isEmpty());

        verify(imagenDao).saveAll(anyList());
    }

    @Test
    void testUpdate() {
        when(imagenDao.save(any(ImagenMongo.class))).thenReturn(crearImagenMongo());
        when(imagenDao.findById(anyString())).thenReturn(Optional.empty());
        when(imagenDao.findById("1234")).thenReturn(getImagenMongo());

        assertThrows(ImageNotFoundException.class, () -> {
            service.update(getImagenDto(), "9999");
        });

        ImagenDto imagen = service.update(getImagenDto(), "1234");
        assertNotNull(imagen);
        verify(imagenDao).save(any(ImagenMongo.class));
    }

    @Test
    void testDelete() {
        doNothing().when(imagenDao).deleteById(anyString());
        doNothing().when(imagenDao).deleteByIdPersona(anyString());
        doThrow(RuntimeException.class).when(imagenDao).deleteById(isNull());
        doThrow(RuntimeException.class).when(imagenDao).deleteByIdPersona(isNull());

        assertThrows(RuntimeException.class, () -> {
            service.delete(null);
        });

        service.delete("1234");

        assertThrows(RuntimeException.class, () -> {
            service.deleteByIdPersona(null);
        });

        service.deleteByIdPersona(1L);

        verify(imagenDao).deleteById(anyString());
        verify(imagenDao).deleteByIdPersona(anyString());
    }
}

package com.pragma.springboot.monolitico.app;

import com.pragma.springboot.monolitico.app.exceptions.ImageNotFoundException;
import com.pragma.springboot.monolitico.app.models.dao.IImagenDao;
import com.pragma.springboot.monolitico.app.models.dto.ImagenDto;
import com.pragma.springboot.monolitico.app.models.entity.Imagen;
import com.pragma.springboot.monolitico.app.models.entity.Persona;
import com.pragma.springboot.monolitico.app.services.IImagenService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.pragma.springboot.monolitico.app.Datos.*;
import static com.pragma.springboot.monolitico.app.Datos.getImagenDto;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class ImagenServiceTest {
    @MockBean
    IImagenDao imagenDao;

    @Autowired
    @Qualifier("mysql")
    IImagenService service;

    @Test
    void testFind() {
        when(imagenDao.findAll()).thenReturn(getListaImagenes());
        when(imagenDao.findById(anyLong())).thenReturn(Optional.empty());
        when(imagenDao.findById(1L)).thenReturn(getImagen());
        doAnswer(invocation -> {
            Persona p = invocation.getArgument(0);
            return p.getId() == 1L ? getListaImagenes() : Collections.emptyList();
        }).when(imagenDao).findByPersona(any(Persona.class));

        List<ImagenDto> imagenes = service.findAll();
        assertNotNull(imagenes);
        assertFalse(imagenes.isEmpty());

        ImagenDto imagen = service.findById("1");
        assertNotNull(imagen);

        ImagenDto imagenNull = service.findById("15");
        assertNull(imagenNull);

        List<ImagenDto> imagenesPersona = service.findByIdPersona(1L);
        assertNotNull(imagenesPersona);
        assertFalse(imagenesPersona.isEmpty());

        List<ImagenDto> listaSinImagenes = service.findByIdPersona(35L);
        assertTrue(listaSinImagenes.isEmpty());
    }

    @Test
    void testSave() {
        when(imagenDao.save(any(Imagen.class))).thenReturn(crearImagen());
        when(imagenDao.saveAll(anyList())).thenReturn(getListaImagenes());

        ImagenDto imagenDto = service.save(getImagenDto());
        assertNotNull(imagenDto);
        verify(imagenDao).save(any(Imagen.class));

        List<ImagenDto> imagenDtos = service.saveAll(getListaImagenesDto());
        assertFalse(imagenDtos.isEmpty());

        List<ImagenDto> listaVacia = service.saveAll(new ArrayList<>());
        assertTrue(listaVacia.isEmpty());

        verify(imagenDao).saveAll(anyList());
    }

    @Test
    void testUpdate() {
        when(imagenDao.save(any(Imagen.class))).thenReturn(crearImagen());
        when(imagenDao.findById(anyLong())).thenReturn(Optional.empty());
        when(imagenDao.findById(1L)).thenReturn(getImagen());

        assertThrows(ImageNotFoundException.class, () -> {
            service.update(getImagenDto(), "9999");
        });

        ImagenDto imagen = service.update(getImagenDto(), "1");
        assertNotNull(imagen);
        verify(imagenDao).save(any(Imagen.class));
    }

    @Test
    void testDelete() {
        doNothing().when(imagenDao).deleteById(anyLong());
        doNothing().when(imagenDao).deleteByPersona(any(Persona.class));
        doThrow(RuntimeException.class).when(imagenDao).deleteById(isNull());

        assertThrows(RuntimeException.class, () -> {
            service.delete(null);
        });

        service.delete("1234");

        assertThrows(IllegalArgumentException.class, () -> {
            service.deleteByIdPersona(null);
        });

        service.deleteByIdPersona(1L);

        verify(imagenDao).deleteById(anyLong());
        verify(imagenDao).deleteByPersona(any(Persona.class));
    }
}
